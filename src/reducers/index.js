import { createStore, combineReducers, applyMiddleware } from 'redux';
import profileReducer from './profile';
import authReducer from './auth';
import botsReducer from './bots';
import thunk from 'redux-thunk';

const rootReducer = combineReducers({ 
  profile: profileReducer,
  auth: authReducer,
  bots: botsReducer, 
});

const configureStore = () => {
    return createStore(rootReducer, applyMiddleware(thunk));
}

export default configureStore;