import React from 'react';
import { Text, View } from 'react-native';

export default ({ children, point, rigth, style, onPress = () => null }) => <Text onPress={onPress} style={{ ...styles.container, ...style }}>
{(!!point && !rigth) && <View style={styles.wrapper}><View style={{ ...styles.point, ...styles[point] }} /></View>}
  {children}
  {(!!point && !!rigth) && <View style={styles.wrapperRigth}><View style={{ ...styles.point, ...styles[point] }} /></View>}
</Text>


const styles = {
  wrapperRigth: {
    width: 15,
    height: 10,
    alignItems: 'flex-end'
  },
  wrapper: {
    width: 15,
    height: 10,
  },
  container: {
    color: 'rgb(185, 187, 190)',
  },
  point: {
    width: 10,
    height: 10,
    backgroundColor: 'rgb(255,255,255)',
    borderRadius: 100,
  },
  success: {
    backgroundColor: '#9bd727'
  },
  warning: {
    backgroundColor: '#facf6a',
  },
  danger: {
    backgroundColor: '#e36c69',
  },
  primary: {
    backgroundColor: '#9a50ff'
  }
};