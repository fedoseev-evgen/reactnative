import { api } from "../index";

export default function getBots({ key, user }) {
  return api.post("https://vto.pe/botcontrol/list", { key, user });
}