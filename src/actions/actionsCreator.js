export function requestActionCreator(name) {
    return {
        request: payload => ({type: name + '/REQUEST', payload}),
        REQUEST: name + '/REQUEST',
        success: payload => ({type: name + '/SUCCESS', payload}),
        SUCCESS: name + '/SUCCESS',
        failure: payload => ({type: name + '/FAILURE', payload}),
        FAILURE: name + '/FAILURE',
    } 
}