import React from 'react';
import { Text } from 'react-native';

export default ({ title, onPress, style, children }) => <Text onPress={onPress} style={{...styles.button, ...style}}>{title?title:children}</Text>;
  

const styles = {
  button: {
    backgroundColor: '#677bc4',
    color: 'rgb(255, 255, 255)',
    borderRadius: 3,
    textAlign: 'center',
    padding: 10,
  }
};