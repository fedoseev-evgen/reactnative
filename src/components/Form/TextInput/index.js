import React from 'react';
import { Text, TextInput, View } from 'react-native';
import P from '../../Text/P';

const CustomTextInput = ({ onChangeText, onBlur, value, label }) => (
  <View style={styles.wrapper}>
    {label && <P>{label}</P>}
    <TextInput
      style={styles.textInput}
      onChangeText={onChangeText}
      onBlur={onBlur}
      value={value}
    />
  </View>
)

const styles = {
  textInput: {
    fontSize: 16,
    lineHeight: 20,
    paddingTop: 5,
    paddingBottom: 5,
    paddingLeft: 10,
    paddingRigth: 10,
    marginTop: 3,
    backgroundColor: 'rgb(64, 68, 75)',
    borderRadius: 8,
    color: 'rgb(220, 221, 222)',
    
  },
  wrapper: {
    marginTop: 10,
  }
};

export default CustomTextInput;