import AsyncStorage from '@react-native-community/async-storage';
import { IS_AUTH } from '../../constants/actions';
import { requestActionCreator } from '../actionsCreator';

export const isAuthActionsCreator = requestActionCreator(IS_AUTH);

export const isAuthRequest = () => {
    return async (dispatch) => {
        try {
            dispatch(isAuthActionsCreator.request());
            const user = await AsyncStorage.getItem('user');
            const key = await AsyncStorage.getItem('key');
            if(user && key) {
                dispatch(isAuthActionsCreator.success({user, key, isAuth: true}));
                return true;
            } else {
                dispatch(isAuthActionsCreator.success({user, key, isAuth: false}));
                throw false;
            }
        } catch(e) {
            dispatch(isAuthActionsCreator.success({user: "", key: "", isAuth: false}));
            throw false;
        }
    }
};