import { GET_PROFILE } from '../../constants/actions';
import { requestActionCreator } from '../actionsCreator';
import { getProfile } from '../../api/profile';

export const getPofileActionsCreator = requestActionCreator(GET_PROFILE);

export const getProfileRequest = () => {
    return async (dispatch, getState) => {
        try {
            dispatch(getPofileActionsCreator.request());
            const { key, user } = getState().auth;
            const response = await getProfile({ key, user });
            dispatch(getPofileActionsCreator.success(response.data));
        } catch (e) {
            console.log(e);
            dispatch(getPofileActionsCreator.failure(e.response.data));
        }
    }
};