import React, { Component } from 'react';
import { View, ScrollView, RefreshControl } from 'react-native';
import NavBar from './NavBar';

export default class MainContainer extends Component {

  render() {
    const { navigate, isLoading, refresh, style, withOutScroll } = this.props;
    return (
      <View style={{
        minHeight: '100%',
        backgroundColor: 'rgb(32, 34, 37)',
        paddingBottom: 60
      }}>
        {
          refresh
            ? <ScrollView
              refreshControl={<RefreshControl colors={['rgb(142,146,148)']} progressBackgroundColor='rgb(32,34,37)' refreshing={isLoading} onRefresh={refresh} />}
              style={{ flex: 1, ...style }}>
              {this.props.children}
            </ScrollView>
            : withOutScroll
              ? <View style={{ flex: 1, ...style }}>{this.props.children}</View>
              : <ScrollView
                style={{ flex: 1, ...style }}>
                {this.props.children}
              </ScrollView>
        }
        {navigate && <NavBar navigate={navigate} />}
      </View>
    );
  }
}
