import { isAuthActionsCreator } from '../../actions/auth/isAuth';
import { loginActionsCreator } from '../../actions/auth/login';
import {SET_AUTH} from '../../constants/actions';
const initialState = {
  isLoading: true,
  isAuth: false,
  key: null,
  user: null,
  errors: {}
};

const authReducer = (state = initialState, action) => {
  switch (action.type) {
    case SET_AUTH:
      return {
        ...state,
        isAuth: action.payload,
      }
    case isAuthActionsCreator.REQUEST:
    case loginActionsCreator.REQUEST:
      return {
        ...state,
        isLoading: true,
        isAuth: false,
        key: "",
        user: "",
        errors: {}
      }
      
    case isAuthActionsCreator.SUCCESS:
    case loginActionsCreator.SUCCESS:
      return {
        ...state,
        isLoading: false,
        isAuth: action.payload.isAuth,
        key: action.payload.key,
        user: action.payload.user,
      }
      
    case isAuthActionsCreator.FAILURE:
    case loginActionsCreator.FAILURE:
      return {
        ...state,
        isLoading: false,
        isAuth: false,
        key: "",
        user: "",
        errors: action.payload
      }
    default:
      return state;
  }
}
export default authReducer;