import { GET_BOT } from '../../constants/actions';
import { getBot } from '../../api/bots';
import { requestActionCreator } from '../actionsCreator';

export const getBotActionsCreator = requestActionCreator(GET_BOT);

export const getBotThunk = (id) => {
    return async (dispatch, getState) => {
        try {
            dispatch(getBotActionsCreator.request());
            const { user, key } = getState().auth;
            const response = await getBot({ user, key, id });
            dispatch(getBotActionsCreator.success(response.data));
            return response.data;
        } catch (e) {
            console.log(e);
            dispatch(getBotActionsCreator.failure(e.response.data));
            throw e.response.data;
        }
    }
};