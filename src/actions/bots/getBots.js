import { GET_BOTS } from '../../constants/actions';
import { getBots } from '../../api/bots';
import { requestActionCreator } from '../actionsCreator';

export const getBotsActionsCreator = requestActionCreator(GET_BOTS);

export const getBotsThunk = () => {
    return async (dispatch, getState) => {
        try {
            dispatch(getBotsActionsCreator.request());
            const { user, key } = getState().auth;
            const response = await getBots({ user, key });
            dispatch(getBotsActionsCreator.success(response.data));
        } catch (e) {
            console.log(e);
            dispatch(getBotsActionsCreator.failure(e.response.data));
        }
    }
};