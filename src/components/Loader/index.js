import React, { useRef, useEffect } from 'react';
import { View, Image, Animated } from 'react-native';
import Spiner from './Spiner';

const Animate = (props) => {
  const fadeAnim = useRef(new Animated.Value(0)).current

  useEffect(() => {
    Animated.loop(
      Animated.timing(
        fadeAnim,
        {
          toValue: 360,
          duration: 1000,
          delay: 0,
          useNativeDriver: true,
        }
      )).start();
  }, [fadeAnim])

  return (
    <Animated.View
      style={{
        transform: [{
          rotate: fadeAnim.interpolate({
            inputRange: [-90, 270],
            outputRange: ['0deg', '360deg']
          })
        }],
      }}
    >
      {props.children}
    </Animated.View>
  );
}


export default ({ children, isLoaded }) => isLoaded
  ? children
  :
  <View style={styles.container}>
    <Spiner/>
  </View>


const styles = {
  container: { 
    position: 'absolute', 
    top: 0, 
    left: 0, 
    right: 0, 
    bottom: 0, 
    justifyContent: 'center', 
    alignItems: 'center'
  },
}