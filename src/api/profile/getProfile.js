import {api} from "../index";

export default function getProfile({key, user}) {
  return api.post("https://vto.pe/api", {key, user, method: "balance"});
}