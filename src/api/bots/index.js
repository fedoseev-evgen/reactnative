import getBots from './getBots';
import getBot from './getBot';

export {getBots, getBot};