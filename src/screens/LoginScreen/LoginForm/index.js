import React from 'react';
import { View } from 'react-native';
import CustomTextInput from '../../../components/Form/TextInput';
import CustomButton from '../../../components/UI/Button';
import { Formik } from 'formik';
import {useSelector, useDispatch} from 'react-redux';
import {loginRequest} from '../../../actions/auth/login';

const LoginForm = () => {
  const dispatch = useDispatch();
  const {user, key} = useSelector(state => state.auth);
  return <Formik
    initialValues={{user, key}}
    onSubmit={values => {
      dispatch(loginRequest(values))
    }}
  >
    {({ handleChange, handleBlur, handleSubmit, values }) => (
      <View>
        <CustomTextInput
          label='user'
          onChangeText={handleChange('user')}
          onBlur={handleBlur('user')}
          value={values.user}
        />
        <CustomTextInput
          label='key'
          onChangeText={handleChange('key')}
          onBlur={handleBlur('key')}
          value={values.key}
        />
        <CustomButton style={{marginTop: 15}} onPress={handleSubmit} title="Войти" />
      </View>
    )}
  </Formik>
};

export default LoginForm;