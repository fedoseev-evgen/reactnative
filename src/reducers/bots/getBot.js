import { getBotActionsCreator } from '../../actions/bots/getBot';

const initialState = {
  isLoading: false,
  bot: {},
  errors: {}
};

const getBotReducer = (state = initialState, action) => {
  switch (action.type) {
    case getBotActionsCreator.REQUEST:
      return {
        ...state,
        isLoading: true,
        bot: {},
        errors: {}
      }
      
    case getBotActionsCreator.SUCCESS:
      return {
        ...state,
        isLoading: false,
        bot: action.payload,
      }
      
    case getBotActionsCreator.FAILURE:
      return {
        ...state,
        isLoading: false,
        errors: action.payload
      }
    default:
      return state;
  }
}
export default getBotReducer;