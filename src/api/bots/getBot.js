import { api } from "../index";

export default function getBots({ key, user, id }) {
  return api.post("https://vto.pe/botcontrol/bot", { key, user, id });
}