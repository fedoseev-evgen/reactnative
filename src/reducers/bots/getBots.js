import { getBotsActionsCreator } from '../../actions/bots/getBots';

const initialState = {
  isLoading: false,
  bots: [],
  earned: {},
  errors: {}
};

const getBotsReducer = (state = initialState, action) => {
  switch (action.type) {
    case getBotsActionsCreator.REQUEST:
      return {
        ...state,
        isLoading: true,
        bots: [],
        earned: {},
        errors: {}
      }
      
    case getBotsActionsCreator.SUCCESS:
      return {
        ...state,
        isLoading: false,
        bots: action.payload.bots,
        earned: action.payload.earned,
      }
      
    case getBotsActionsCreator.FAILURE:
      return {
        ...state,
        isLoading: false,
        errors: action.payload
      }
    default:
      return state;
  }
}
export default getBotsReducer;