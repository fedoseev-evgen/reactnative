import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Text, View } from 'react-native';
import MainContainer from '../../components/MainContainer';
import Card from '../../components/Card';
import Row from '../../components/Grid/Row';
import Col from '../../components/Grid/Col';
import Container from '../../components/Container';
import { getProfileRequest } from '../../actions/profile/getProfile';
import { getBotsThunk } from '../../actions/bots/getBots';
import P from '../../components/Text/P';

export default ({ navigation }) => {

  const dispatch = useDispatch();
  const { earned, bots, isLoading: isLoadingBots } = useSelector(state => state.bots.getBots);
  const { balance, isLoading: isLoadingProfile } = useSelector(state => state.profile.getProfile);

  const isLoadingPage = isLoadingBots || isLoadingProfile;
  const refreshScreen = () => {
    dispatch(getProfileRequest());
    dispatch(getBotsThunk());
  }
  useEffect(() => {
    dispatch(getBotsThunk());
  }, []);

  const { navigate, push } = navigation;
  return (
    <MainContainer refresh={refreshScreen} isLoading={isLoadingPage} navigate={navigate}>
      <Container>
        <Row>
          <Col>
            <Card style={{ margin: 0 }}>
              <P>Баланс: {balance}</P>
              <P>Заработано всего: {earned.total}</P>
              <P>Заработано за день: {earned.today}</P>
              <P>Заработано за неделю: {earned.week}</P>
              <P>Заработано за месяц: {earned.month}</P>
            </Card>
          </Col>
        </Row>
        <Text style={styles.title}>Список ботов:</Text>
        <View style={styles.content}>
          {bots.map(bot => <Card key={bot.id} onPress={() => {push('SingleBot', {id: bot.id})}}>
            <View style={styles.bot}>
              <P point={`${bot.access === 'on' ? 'success' : 'danger'}`}>{bot.name}</P>
              <View style={styles.statuses}>
                <P rigth point="success" style={styles.status}>{bot.accounts.success}</P>
                <P rigth point="primary" style={styles.status}>{bot.accounts.primary}</P>
                <P rigth point="warning" style={styles.status}>{bot.accounts.warning}</P>
                <P rigth point="danger" style={styles.status}>{bot.accounts.danger}</P>
              </View>
            </View>
          </Card>)}
        </View>
      </Container>
    </MainContainer>
  );
}

const styles = {
  status: {
    marginLeft: 8,
  },
  statuses: {
    flexDirection: 'row'
  },
  image: {
    width: 50,
    height: 50,
  },
  minContainer: {
    minHeight: '100%',
    backgroundColor: 'rgb(32, 34, 37)'
  },
  text: {
    color: 'rgb(185, 187, 190)',
  },
  title: {
    color: 'rgb(185, 187, 190)',
    fontSize: 20,
    marginTop: 10,
    marginBottom: 5
  },
  bot: {
    flexDirection: 'row',
    justifyContent: 'space-between'
  },
  content: {
    minHeight: 100,
  },
}