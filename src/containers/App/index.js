import React, { useEffect } from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import app from "../../routes/app";
import { navigationRef } from "../../constants/root-navigation";
import LoginScreen from "../../screens/LoginScreen";
import { isAuthRequest } from "../../actions/auth/isAuth";
import { getProfileRequest } from "../../actions/profile/getProfile";
import { useSelector, useDispatch } from 'react-redux';

const Stack = createStackNavigator();

function App() {
  const { isAuth, isLoading } = useSelector(state => state.auth);
  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(isAuthRequest())
    .then(() => {
      dispatch(getProfileRequest());
    });
  }, []);
  return (
    isAuth && !isLoading ?
      <NavigationContainer ref={navigationRef}>
        <Stack.Navigator initialRouteName='start'>
          {
            app.map((route, index) => <Stack.Screen
              key={index}
              name={route.name}
              component={route.component}
              options={route.options}
            />)
          }
        </Stack.Navigator>
      </NavigationContainer> :
      <NavigationContainer ref={navigationRef}>
        <Stack.Navigator initialRouteName='start'>
          <Stack.Screen
            name="LoginScreen"
            component={LoginScreen}
            options={{
              title: 'Логин',
              headerTintColor: 'rgb(142,146,148)',
              headerStyle: {
                backgroundColor: 'rgb(32,34,37)',
              },
            }}
          />
        </Stack.Navigator>
      </NavigationContainer>
  );
}

export default App;
