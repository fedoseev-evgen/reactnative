export const IS_AUTH = 'IS_AUTH';
export const SET_AUTH = 'SET_AUTH';
export const LOGIN = 'LOGIN';
export const GET_BOTS = 'GET_BOTS';
export const GET_BOT = 'GET_BOT';
export const GET_PROFILE = 'GET_PROFILE';