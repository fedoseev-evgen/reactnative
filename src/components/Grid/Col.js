import React from 'react';
import { View } from 'react-native';

export default ({ children }) => <View style={styles.container}>
  {children}
</View>


const styles = {
  container: {
    flexGrow: 1,
    padding: 10,
    width: 0
  }
};