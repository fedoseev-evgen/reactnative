import React, { useRef, useEffect } from 'react';
import { View, Image, Animated } from 'react-native';
import LoadingIcon from '../../../media/images/loading.png';

const Animate = (props) => {
    const fadeAnim = useRef(new Animated.Value(0)).current

    useEffect(() => {
        Animated.loop(
            Animated.timing(
                fadeAnim,
                {
                    toValue: 360,
                    duration: 1000,
                    delay: 0,
                    useNativeDriver: true,
                }
            )).start();
    }, [fadeAnim])

    return (
        <Animated.View
            style={{
                transform: [{
                    rotate: fadeAnim.interpolate({
                        inputRange: [-90, 270],
                        outputRange: ['0deg', '360deg']
                    })
                }],
            }}
        >
            {props.children}
        </Animated.View>
    );
}


export default ({ style }) =>
    <Animate>
        <Image style={{ ...styles.image, ...style }} source={LoadingIcon} />
    </Animate>


const styles = {
    image: {
        width: 50,
        height: 50,
    },
}