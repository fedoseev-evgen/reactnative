import React from 'react';
import { View } from 'react-native';
import LoginForm from './LoginForm';
import MainContainer from '../../components/MainContainer';

export default () => {
  return (
    <MainContainer style={styles.constainer} withOutScroll>
      <View style={styles.formBox}>
        <LoginForm />
      </View>
    </MainContainer>
  );
}

const styles = {
  constainer: {
    alignItems: 'center',
    justifyContent: 'center',
  },
  formBox: {
    margin: 'auto',
    // marginBottom: 'auto',
    padding: 20,
    backgroundColor: 'rgb(54, 57, 63)',
    width: '80%',
  }
}