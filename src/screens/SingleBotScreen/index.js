import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Text, View } from 'react-native';
import MainContainer from '../../components/MainContainer';
import Card from '../../components/Card';
import Row from '../../components/Grid/Row';
import Col from '../../components/Grid/Col';
import Container from '../../components/Container';
import { getProfileRequest } from '../../actions/profile/getProfile';
import { getBotThunk } from '../../actions/bots/getBot';
import P from '../../components/Text/P';
import { getStatusAccountString, getStatusAccountType, getStatusAccountSocialNetwork } from '../../utils/getAccountInfo';

export default ({ navigation, route }) => {

  const dispatch = useDispatch();
  const { bot, isLoading: isLoadingBot } = useSelector(state => state.bots.getBot);

  const { id } = route.params;
  const { navigate } = navigation;
  const refreshScreen = () => {
    dispatch(getProfileRequest());
  }
  useEffect(() => {
    dispatch(getBotThunk(id))
      .then((bot) => {
        navigation.setOptions({
          title: `Бот: ${bot.name}`
        })
      });
  }, [id]);

  return (
    <MainContainer refresh={refreshScreen} isLoading={isLoadingBot} navigate={navigate}>
      <Container>
        <Row>
          <Col>
            <Card style={{ margin: 0 }}>
              <P>Бот заработал за день: {bot.earned?.today}</P>
              <P>Бот заработал за неделю: {bot.earned?.week}</P>
              <P>Бот заработал за месяц: {bot.earned?.month}</P>
            </Card>
          </Col>
        </Row>

        <View style={styles.content}>
          {bot.accounts?.map((account, index) => <Card key={account.id}>
            <View style={styles.account}>
              <View style={styles.split}>
                <P point={getStatusAccountType(account.status)}>{account.login}</P>
                <P style={styles.status}>День: {account.earned.day}/ Всего: {account.earned.total}</P>
              </View>
              <View style={styles.split}>
                <P>{getStatusAccountString(account.status)}</P>
                <P>{getStatusAccountSocialNetwork(account.service)}</P>
              </View>
            </View>
          </Card>)}
        </View>
      </Container>
    </MainContainer>
  );
}

const styles = {
  split: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  }
}