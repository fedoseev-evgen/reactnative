export function getStatusAccountString(status) {
  switch (status) {
    case 'manual':
      return 'Ручная авторизация';
    case 'ipv6proxy':
      return 'Соцсеть не поддерживает IPv6';
    case 'loaderrormobilecode':
      return 'Специальная авторизация';
    case 'duplicate':
      return 'Дубликат аккаунта';
    case 'banned':
      return 'Заблокирован';
    case 'badauth':
      return 'Неверные логин/пароль';
    case 'badproxy':
      return 'Привязанный прокси не работает';
    case 'level_0':
      return 'Аккаунт 0 уровня';
    case 'nonvalidating':
      return 'Ожидает проверки';
    case 'validating':
      return 'Автоматическая проверка';
    case 'tomanual':
      return 'Автоматическая проверка';
    case 'tomanualmobilecode':
      return 'Автоматическая проверка';
    case 'prevalidating':
      return 'Автоматическая проверка';
    case 'badpreauth':
      return 'Автоматическая проверка';
    case 'okpreauth':
      return 'Автоматическая проверка';
    case 'waitproxy':
      return 'Аккаунт ожидает прокси';
    case 'queued':
      return 'В очереди на работу';
    case 'nonworking':
      return 'Ожидает работы';
    case 'working':
      return 'В работе';
    case 'toinfo':
      return 'В работе';
    case 'workingblocked':
      return 'Блок на действия со стороны соцсети';
    default:
      return 'Аккаунт отдыхает'
  }
}

export function getStatusAccountType(status) {
  let type = 'warning';
  if (status === 'working' || status === 'toinfo') {
    type = 'success';
  }
  return type;
}

export function getStatusAccountSocialNetwork(service) {
  switch (service) {
    case 'v':
      return 'ВКонтакте';
    case 't':
      return 'Twitter';
    case 'i':
      return 'Instagram';
    case 'a':
      return 'Ask.fm';
    case 'y':
      return 'Youtube';
    case 'o':
      return 'Ok.ru';
    case 'm':
      return 'Telegram';
    default:
      return `Я не знаю что такое ${service}`;
  }
}