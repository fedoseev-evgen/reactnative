import { getPofileActionsCreator } from '../../actions/profile/getProfile';

const initialState = {
  isLoading: true,
  balance: 0,
  errors: {}
};

const getProfileReducer = (state = initialState, action) => {
  switch (action.type) {
    case getPofileActionsCreator.REQUEST:
      return {
        ...state,
        isLoading: true,
        balance: 0,
        errors: {}
      }
      
    case getPofileActionsCreator.SUCCESS:
      return {
        ...state,
        isLoading: false,
        balance: action.payload.balance,
      }
      
    case getPofileActionsCreator.FAILURE:
      return {
        ...state,
        isLoading: false,
        errors: action.payload,
      }
    default:
      return state;
  }
}
export default getProfileReducer;