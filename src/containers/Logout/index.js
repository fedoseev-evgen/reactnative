import React, { useEffect, useState } from 'react';
import { useDispatch } from 'react-redux';
import { authActionsCreator } from '../../actions/auth/setAuth';
import P from '../../components/Text/P';

export default () => {

  const dispatch = useDispatch();

  const logout = () => {
    dispatch(authActionsCreator.setAuth(false));
  }

  return (
    <P style={styles.button} onPress={logout}>Выход</P>
  );
}

const styles = {
  button: {
    marginRight: 20
  }
}