import React from 'react';
import { TouchableOpacity, View } from 'react-native';

const Card = ({ children, style, onPress }) => {
    return (onPress ?
        <TouchableOpacity onPress={onPress} style={{ ...styles.card, ...style }}>
            {children}
        </TouchableOpacity> :
        <View style={{ ...styles.card, ...style }}>
            {children}
        </View>
    );
}

const styles = {
    card: {
        padding: 10,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5,
        backgroundColor: 'rgb(47, 49, 54)',
        // padding: '10',
        marginTop: 10,
    }
}

export default Card;