import AsyncStorage from '@react-native-community/async-storage';
import { LOGIN } from '../../constants/actions';
import { requestActionCreator } from '../actionsCreator';

export const loginActionsCreator = requestActionCreator(LOGIN);

export const loginRequest = ({user, key}) => {
  return async (dispatch) => {
    try {
      dispatch(loginActionsCreator.request());
      if (user && key) {
        await AsyncStorage.setItem('user', user);
        await AsyncStorage.setItem('key', key);
        dispatch(loginActionsCreator.success({ user, key, isAuth: true }));
      } else {
        dispatch(loginActionsCreator.success({ user: '', key: '', isAuth: false }));
      }
    } catch (e) {
      dispatch(loginActionsCreator.success({ user: "", key: "", isAuth: false }));
    }
  }
};