import React from 'react';
import MainScreen from "../screens/MainScreen";
import SingleBotScreen from "../screens/SingleBotScreen";
import Logout from "../containers/Logout";

export default [
    {
        name: "Start",
        component: MainScreen,
        options: {
            title: 'Главная',
            headerTintColor: 'rgb(142,146,148)',
            headerStyle: {
                backgroundColor: 'rgb(32,34,37)',
            },
            headerRight: () => <Logout/>,
        },
    },
    {
        name: "SingleBot",
        component: SingleBotScreen,
        options: {
            title: 'Бот',
            headerTintColor: 'rgb(142,146,148)',
            headerStyle: {
                backgroundColor: 'rgb(32,34,37)',
            },
            headerRight: () => <Logout/>,
        },
    },
    // {
    //     name: "LoginScreen",
    //     component: LoginScreen,
    //     options: {
    //         headerTintColor: 'rgb(142,146,148)',
    //         headerStyle: {
    //             backgroundColor: 'rgb(32,34,37)',
    //         },
    //     },
    // },
    // {
    //     name: "ScreenB",
    //     component: ScreenB,
    //     options: {},
    // },
]
