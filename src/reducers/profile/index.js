import { combineReducers } from 'redux';
import getProfileReducer from './getProfile';

const rootReducer = combineReducers({ 
  getProfile: getProfileReducer,
});

export default rootReducer;