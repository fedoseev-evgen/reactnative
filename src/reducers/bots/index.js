import { combineReducers } from 'redux';
import getBotsReducer from './getBots';
import getBotReducer from './getBot';

const rootReducer = combineReducers({ 
  getBots: getBotsReducer,
  getBot: getBotReducer,
});

export default rootReducer;