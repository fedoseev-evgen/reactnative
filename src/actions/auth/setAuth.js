import { SET_AUTH } from '../../constants/actions';

export const authActionsCreator = {
  setAuth: payload => ({
    type: SET_AUTH,
    payload,
  })
};
